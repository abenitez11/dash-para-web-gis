import dash
import dash_leaflet as dl
import dash_bootstrap_components as dbc
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.express as px
import requests
import pandas as pd

# URLs del servicio WMS de GeoServer
wms_url_terra = "http://geoserver.girsar/geoserver/MODIS_TERRA/wms?"
wms_url_aqua = "http://geoserver.girsar/geoserver/MODIS_AQUA/wms?"

# URL del servicio WFS de GeoServer para extraer datos
wfs_url_combined = "http://geoserver.girsar/geoserver/modis_combined/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=modis_combined%3Amodis_combined&maxFeatures=50&outputFormat=application%2Fjson"

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
app.title = "Visualización de Datos MODIS Terra-Aqua"

app.layout = dbc.Container(fluid=True, children=[
    dbc.Row([
        dbc.Col(html.H1("Visualización de Datos MODIS Terra-Aqua", className="text-center my-4"), width=12)
    ]),
    dbc.Row([
        dbc.Col([
            html.Label("Seleccionar Rango de Fechas:"),
            dcc.DatePickerRange(
                id='fecha-range',
                start_date_placeholder_text="Inicio",
                end_date_placeholder_text="Fin",
                start_date=None,
                end_date=None,
                className="mb-3"
            ),
            html.Label("Seleccionar Satélite:"),
            dcc.Dropdown(
                id='satelite-dropdown',
                options=[
                    {'label': 'MODIS TERRA', 'value': 'MODIS_TERRA'},
                    {'label': 'MODIS AQUA', 'value': 'MODIS_AQUA'}
                ],
                multi=True,
                value=[],
                className="mb-3"
            ),
            html.Div(id='fecha-lista')
        ], md=4),
        dbc.Col([
            dl.Map(center=[0, 0], zoom=2, children=[dl.TileLayer()], id='mapa-leaflet', style={'width': '100%', 'height': '500px'})
        ], md=8)
    ]),
    dbc.Row([
        dbc.Col(dcc.Graph(id='grafico-barras'), md=4),
        dbc.Col(dcc.Graph(id='grafico-caja'), md=4),
        dbc.Col(dcc.Graph(id='grafico-autogenerado'), md=4),
    ], className="my-4"),
    dbc.Row([
        dbc.Col(html.Div(id='estadisticas-container'), md=12),
    ], className="my-4")
])

# Función para obtener y procesar datos de WFS
def obtener_y_procesar_datos_wfs(url, satelite_seleccionado, start_date, end_date):
    response = requests.get(url)
    if response.status_code == 200:
        datos = response.json()
        df = pd.json_normalize(datos['features'])
        # Filtrar df por satélite y fechas
        return df
    else:
        return pd.DataFrame()
    
# Función para calcular estadísticas
def calcular_estadisticas(df):
    estadisticas = {
        "Media de FP_POWER": df["properties.FP_POWER"].mean(),
        "Max de FP_CONFIDENCE": df["properties.FP_CONFIDENCE"].max(),
        # Agregar aquí más cálculos según sea necesario
    }
    return estadisticas

@app.callback(
    Output('mapa-leaflet', 'children'),
    [Input('satelite-dropdown', 'value'), Input('fecha-range', 'start_date'), Input('fecha-range', 'end_date')]
)
def actualizar_mapa(satelite_seleccionado, start_date, end_date):
    mapa_children = [dl.TileLayer()]
    if 'MODIS_TERRA' in satelite_seleccionado and satelite_valido(start_date, end_date, 'MODIS_TERRA'):
        mapa_children.append(dl.WMSTileLayer(
            url=wms_url_terra, layers="MODIS_TERRA:MODIS_TERRA",
            format="image/png", transparent=True, version="1.1.0",
            attribution="GIS Data"))
    if 'MODIS_AQUA' in satelite_seleccionado and satelite_valido(start_date, end_date, 'MODIS_AQUA'):
        mapa_children.append(dl.WMSTileLayer(
            url=wms_url_aqua, layers="MODIS_AQUA:MODIS_AQUA",
            format="image/png", transparent=True, version="1.1.0",
            attribution="GIS Data"))
    return mapa_children

def satelite_valido(start_date, end_date, satelite):
    if not start_date or not end_date:
        return False
    # Aquí puedes agregar más lógica de validación si es necesario
    return True

@app.callback(
    [Output('grafico-barras', 'figure'),
     Output('grafico-caja', 'figure'),
     Output('grafico-autogenerado', 'figure'),
     Output('estadisticas-container', 'children')],
    [Input('satelite-dropdown', 'value'), Input('fecha-range', 'start_date'), Input('fecha-range', 'end_date')]
)
def actualizar_graficos(satelite_seleccionado, start_date, end_date):
    if not satelite_valido(start_date, end_date, satelite_seleccionado):
        return [px.bar(), px.box(), px.scatter(), "Seleccione un rango de fechas y satélite válido."]
    
    df = obtener_y_procesar_datos_wfs(wfs_url_combined, satelite_seleccionado, start_date, end_date)
    estadisticas = calcular_estadisticas(df)

    fig_barras = px.bar(df, x='properties.Fecha', y='properties.FP_POWER', title='FP_POWER a lo largo del tiempo')
    fig_caja = px.box(df, y='properties.FP_POWER', title='Distribución de FP_POWER')
    fig_autogenerado = px.scatter(df, x='properties.Longitud', y='properties.Latitud', color='properties.FP_POWER', title='FP_POWER por Ubicación')
    
    estadisticas_html = html.Div([
        html.H4("Estadísticas Combinadas"),
        html.Ul([html.Li(f"{key}: {value}") for key, value in estadisticas.items()])
    ])

    return fig_barras, fig_caja, fig_autogenerado, estadisticas_html


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=8000)
)